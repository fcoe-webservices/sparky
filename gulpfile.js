//// Gulpfile Version 0.9
// For use with Zurb Foundation in Drupal 9

const {src, dest, watch, series} = require('gulp');
const sass = require('gulp-sass')(require('sass-embedded'));
const postcss = require('gulp-postcss');
const cleancss = require('postcss-clean');
const terser = require('gulp-terser');
const browser = require('browser-sync').create();

//// Load settings from config.yml
// Updated method used based on zurb template
const yaml = require('js-yaml');
const fs = require('fs-extra');

function loadConfig() {
    const unsafe = require('js-yaml-js-types').all;
    const schema = yaml.DEFAULT_SCHEMA.extend(unsafe);
    const ymlFile = fs.readFileSync('config.yml', 'utf8');
    return yaml.load(ymlFile, {schema});
  }

const { BROWSERSYNC, PATHS } = loadConfig();
console.log(PATHS);

//// Gulp Sass Build Task
// Build scss -> css with sass-embedded then stream
function buildTask(){
    return src(PATHS.scss, {sourcemaps: true })
        .pipe(sass({
            includePaths: PATHS.foundationScss
        }).on('error', sass.logError))
        .pipe(postcss([cleancss()]))
        .pipe(dest('css'))
        .pipe(browser.stream());
}

//// Javascript Build Task
// Using terser to compile
function jsTask(){
    return src(PATHS.js, {sourcemaps: true })
        .pipe(terser())
        .pipe(dest('dist', {sourcemaps: '.' }));
}

//// Browsersync Initialize Server Task
// Initializes Browsersync server with project URL
function browserServe(cb) {
    browser.init({
        host: BROWSERSYNC,
        proxy: {
            target: BROWSERSYNC
        },
        open: false,
        ui: false
    }, cb());
}

//// Browsersync Reload Webpage Function
// Completely refreshes the page across browsers
function browserReload(cb){
    browser.reload();
    cb();
}

//// Gulp Watch Setup
function watchTask(){
    watch(PATHS.twig, browserReload);
    // If @imports or $variables are changed then a page reload is needed
    watch(PATHS.scss, series(buildTask, browserReload));
    // If scss is changed then compile and stream
    watch(PATHS.componentScss, buildTask);
    // If js is changed then a page reload is needed
    watch(PATHS.js, series(jsTask, browserReload));
}

//// Run Gulp Tasks
// ddev yarn run gulp
exports.default = series(
    jsTask,
    buildTask,
    browserServe,
    watchTask
)
// ddev yarn run watch
exports.watch = series(
    browserServe,
    watchTask
)


/* OLD GULPFILE */
// var gulp            = require('gulp'),
//     $               = require('gulp-load-plugins')(),
//     browserSync     = require('browser-sync'),
//     //reload          = browserSync.reload,
//     importOnce      = require('node-sass-import-once');

//     // pathing
//     sassPaths = [
//         'bower_components/foundation-sites/scss',
//         'bower_components/motion-ui/src',
//         'components/'
//     ],

//     // Error notifications in console
//     reportError = function(error) {
//         $.notify({
//             title: 'Gulp Task Error',
//             message: 'Check the console.'
//         }).write(error);
//         console.log(error.toString());
//         this.emit('end');
//     };


// var sassFiles = [
//   'components/**/*.scss',
//   // Do not open Sass partials as they will be included as needed.
//   '!components/**/_*.scss',
//   // Chroma markup has its own gulp task.
//   '!components/style-guide/kss-example-chroma.scss'
// ];

// // Sass processing
// gulp.task('sass', function() {
//   return gulp.src(sassFiles)
//     //.pipe($.sassGlob())
//     .pipe($.sass({
//         importer: importOnce,

//         includePaths: sassPaths
//     })
//     .on('error', $.sass.logError))
//     .pipe($.sourcemaps.init())
//     .pipe($.sass({
//         outputStyle: 'nested', // libsass doesn't support expanded yet
//         precision: 10
//     }))
//     .on('error', reportError)
//     .pipe($.autoprefixer({
//         browsers: ['last 2 versions', 'ie >= 9']
//     }))
//     .pipe($.rename({dirname: ''}))
//     .pipe($.sourcemaps.write())
//     .pipe(gulp.dest('components/build/css'))
//     .pipe(browserSync.stream());
// });

// // Run drush to clear views
// // using the drush-reload task to reload the browser
// // we follow this path so that the reload will only kick in once the task is complete
// gulp.task('drush-reload', ['drush'], browserSync.reload);
// gulp.task('drush', function() {
//     return gulp.src('', {
//         read: false
//     })
//     .pipe($.shell([
//         //'drush cache-clear views',
//     ]));
// });

// // BrowserSync
// gulp.task('browser-sync', function() {
//     browserSync.init({
//       ui: {
//         port: 4001
//       },
//       port: 4000,
//       proxy: "https://sparkfresno.lndo.site"
//     });
// });

// // The following tasks are in development

// // Optimize Images
// gulp.task('images', function() {
//     return gulp.src('images/**/*')
//     .pipe($.imagemin({
//         progressive: true,
//         interlaced: true,
//         svgoPlugins: [{
//             cleanupIDs: false
//         }]
//     }))
//     .pipe(gulp.dest('images'));
// });

// // JS hint
// gulp.task('jshint', function() {
//     return gulp.src('scripts/*.js')
//     .pipe(reload({
//         stream: true,
//         once: true
//     }))
//     .pipe($.jshint())
//     .pipe($.jshint.reporter('jshint-stylish'))
//     .pipe($.notify({
//         title: "JS Hint",
//         message: "JS Hint says all is good.",
//         onLast: true
//     }));
// });

// // Beautify JS
// gulp.task('beautify', function() {
//     gulp.src('scripts/*.js')
//     .pipe($.beautify({indentSize: 2}))
//     .pipe(gulp.dest('scripts'))
//     .pipe($.notify({
//         title: "JS Beautified",
//         message: "JS files in the theme have been beautified.",
//         onLast: true
//     }));
// });

// // Compress JS
// gulp.task('compress', function() {
//     return gulp.src('scripts/*.js')
//     .pipe($.sourcemaps.init())
//     .pipe($.uglify())
//     .pipe($.sourcemaps.write())
//     .pipe(gulp.dest('scripts'))
//     .pipe($.notify({
//         title: "JS Minified",
//         message: "JS files in the theme have been minified.",
//         onLast: true
//     }));
// });

// gulp.task('default', ['sass', 'browser-sync'], function() {
//     gulp.watch(['components/**/*.scss','components/**/*.twig'], ['sass']);
//     gulp.watch(['templates/**/*.twig'], ['drush-reload']);
// });

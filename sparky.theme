<?php

/**
 * Implements hook_page_attachments().
 */

function sparky_page_attachments_alter(array &$page) {
  $httpEquiv = [
    '#tag' => 'meta',
    '#attributes' => [
      'http-equiv' => 'X-UA-Compatible',
      'content' => 'IE=edge',
    ],
  ];
  $manifest = [
    '#tag' => 'meta',
    '#attributes' => [
      'rel' => 'manifest',
      'content' => 'manifest.json',
    ],
  ];
  $msapplicationTapHighlight = [
    '#tag' => 'meta',
    '#attributes' => [
      'name' => 'msapplication-tap-highlight',
      'content' => 'no',
    ],
  ];
  $mobileWebAppCapable = [
    '#tag' => 'meta',
    '#attributes' => [
      'name' => 'mobile-web-app-capable',
      'content' => 'yes',
    ],
  ];
  $applicationName = [
    '#tag' => 'meta',
    '#attributes' => [
      'name' => 'application-name',
      'content' => 'FCOE.org',
    ],
  ];
  $icon = [
    '#tag' => 'meta',
    '#attributes' => [
      'rel' => 'icon',
      'content' => 'images/touch/chrome192x192.png',
    ],
  ];
  $androidThemeColor = [
    '#tag' => 'meta',
    '#attributes' => [
      'rel' => 'theme-color',
      'content' => '#193365',
    ],
  ];
  $appleMobileWebAppCapable = [
    '#tag' => 'meta',
    '#attributes' => [
      'name' => 'apple-mobile-web-app-capable',
      'content' => 'yes',
    ],
  ];
  $appleMobileWebAppStatusBarStyle = [
    '#tag' => 'meta',
    '#attributes' => [
      'name' => 'apple-mobile-web-app-status-bar-style',
      'content' => 'black',
    ],
  ];
  $appleMobileWebAppTitle = [
    '#tag' => 'meta',
    '#attributes' => [
      'name' => 'apple-mobile-web-app-title',
      'content' => 'FCOE.org',
    ],
  ];
  $appleTouchIcon = [
    '#tag' => 'meta',
    '#attributes' => [
      'rel' => 'apple-touch-icon',
      'content' => 'images/touch/apple.png',
    ],
  ];

  $page['#attached']['html_head'][] = [$manifest, 'manifest'];
  $page['#attached']['html_head'][] = [$httpEquiv, 'http-equiv'];
  $page['#attached']['html_head'][] = [$msapplicationTapHighlight, 'msapplication-tap-highlight'];

  /* for Chrome on Android */
  $page['#attached']['html_head'][] = [$mobileWebAppCapable, 'mobile-web-app-capable'];
  $page['#attached']['html_head'][] = [$applicationName, 'applicationName'];
  $page['#attached']['html_head'][] = [$icon, 'icon'];

  /* for Safari on iOS */
  $page['#attached']['html_head'][] = [$appleMobileWebAppCapable, 'apple-mobile-web-app-capable'];
  $page['#attached']['html_head'][] = [$appleMobileWebAppStatusBarStyle, 'apple-mobile-web-app-status-bar-style'];
  $page['#attached']['html_head'][] = [$appleMobileWebAppTitle, 'apple-mobile-web-app-title'];
  $page['#attached']['html_head'][] = [$appleTouchIcon, 'apple-touch-icon'];
}

/**
 * Implements hook_css_alter().
 *
 * Browsersync does not work with CSS import so we need to force Drupal to embed
 * CSS files as <link> elements.
 *
 * @link https://github.com/shakyShane/browser-sync/issues/10
 */

function sparky_css_alter(&$css) {
  $system_css_preprocess = \Drupal::config('system.performance')->get('css.preprocess');

  if ( !$system_css_preprocess) {
    foreach ($css as $key => $value) {
      // Skip core files.
      if (strpos($value['data'], 'core/') !== 0) {
        $css[$key]['preprocess'] = FALSE;
      }
    }
  }
}

use Drupal\Core\Url;

/**
 * Implements hook_preprocess_block().
 */
function sparky_preprocess_block(&$variables) {
  if( isset($variables['elements']['content']['#name']) ){
  	$variables['h2_class'] = $variables['elements']['content']['#name'];
  }
  $variables['content']['#attributes']['block'] = $variables['attributes']['id'];

  if ( isset($variables['elements']['#id']) && ( strpos($variables['elements']['#id'], 'section_nav') !== false )) {
    $menu_link_manager = \Drupal::service('plugin.manager.menu.link');
    $trailIds = \Drupal::service('menu.active_trail')->getActiveTrailIds('main');
    end($trailIds);
    $variables['configuration']['label'] = $menu_link_manager->createInstance( prev($trailIds) )->getTitle() . " Menu";
  }
}

function sparky_theme_suggestions_block_alter(&$suggestions, $variables) {
  $content = $variables['elements']['content'];
  if (isset($content['#block_content']) && $content['#block_content'] instanceof \Drupal\block_content\BlockContentInterface) {
    $suggestions[] = 'block__' . $content['#block_content']->bundle();
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function sparky_theme_suggestions_menu_alter(array &$suggestions, array $variables) {
  // Remove the block and replace dashes with underscores in the block ID to
  // use for the hook name.
  if (isset($variables['attributes']['block'])) {
    $hook = str_replace(array('block-', '-'), array('', '_'), $variables['attributes']['block']);
    $suggestions[] = $variables['theme_hook_original'] . '__' . $hook;
  }
}


function sparky_preprocess_field(&$variables, $hook) {
  // Make additional variables available to the template.
  $variables['bundle'] = $variables['element']['#bundle'];
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function sparky_theme_suggestions_image_formatter_alter(array &$suggestions, array $variables) {
  $entity = $variables['item']->getEntity();
  $field_name = $variables['item']->getParent()->getName();
  $suggestions[] = 'image_formatter__' . $field_name;
}

/**
 * Implementation of hook_editor_js_settings_alter().
 * @param array $settings
 */
function sparky_editor_js_settings_alter(array &$settings) {
  // Change height of the ckeditor wysiwyg editor.
  if (isset($settings['editor']['formats']['basic_html'])) {
    $settings['editor']['formats']['basic_html']['editorSettings']['height'] = '400';
  }
}

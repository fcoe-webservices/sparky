/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 *
 */
(function ($, Drupal) {

  /**
   * Use this behavior as a template for custom Javascript.
   */
  Drupal.behaviors.exampleBehavior = {
    attach: $("a.video-link").on("click", function(event) {
        event.preventDefault();
        console.log("func 1");
        vall = $(this).attr("href");
        $("iframe").attr('src', "//www.youtube.com/embed/sxkgaDhRzTo" + "?autoplay=0" + "&modestbranding=0" + "&showinfo=0" + "&rel=0");
        $("#videoModal").foundation("open");
      }),
    attach: $(document).on("closed.zf.reveal", '[data-reveal]', function () {
        $(".frontpage-video iframe").attr('src', '');
        var ytc = $(".youtube-video").length;
        for (i=0; i < ytc; i++) {
          $(".youtube-video")[i].contentWindow.postMessage('{"event": "command", "func":"' + 'stopVideo' + '", "args":""}', '*');
        }
      }),
    attach: $("a.video-link-2").on("click", function(event) {
        event.preventDefault();
        console.log("console 2");
        vall = $(this).attr("href");
        $("iframe").attr('src', "//www.youtube.com/embed/UIiV1mipsGU" + "?autoplay=0" + "&modestbranding=0" + "&showinfo=0" + "&rel=0");
        $('#second_videoModal').foundation('open');
      }),
    attach: $(document).on("closed.zf.reveal", '[data-reveal]', function () {
        $(".frontpage-video-second iframe").attr('src', '');
        var ytc = $(".youtube-video").length;
        for (i=0; i < ytc; i++) {
          $(".youtube-video")[i].contentWindow.postMessage('{"event": "command", "func":"' + 'stopVideo' + '", "args":""}', '*');
        }
      }),
    attach: $("a.art-social-emotional").on("click", function(event) {
        event.preventDefault();
        vall = $(this).attr("href");
        $("iframe").attr('src', "//www.youtube.com/embed/MxbOao9uLrU" + "?autoplay=0" + "&modestbranding=0" + "&showinfo=0" + "&rel=0");
        $('#videoModalArtSocialEmotional').foundation('open');
      }),
    attach: $(document).on("closed.zf.reveal", '[data-reveal]', function () {
        $(".art-social-emotional iframe").attr('src', '');
        var ytc = $(".youtube-video").length;
        for (i=0; i < ytc; i++) {
          $(".youtube-video")[i].contentWindow.postMessage('{"event": "command", "func":"' + 'stopVideo' + '", "args":""}', '*');
        }
      }),
    attach: $("a.arts-equity").on("click", function(event) {
        event.preventDefault();
        vall = $(this).attr("href");
        $("iframe").attr('src', "//www.youtube.com/embed/nvGcrpTA5A4" + "?autoplay=0" + "&modestbranding=0" + "&showinfo=0" + "&rel=0");
        $('#videoModalArtsEquity').foundation('open');
      }),
    attach: $(document).on("closed.zf.reveal", '[data-reveal]', function () {
        $('.arts-equity iframe').attr('src', '');
        var ytc = $(".youtube-video").length;
        for (i=0; i < ytc; i++) {
          $(".youtube-video")[i].contentWindow.postMessage('{"event": "command", "func":"' + 'stopVideo' + '", "args":""}', '*');
        }
      }),
    attach: $("a.arts-early-childhood").on("click", function(event) {
        event.preventDefault();
        vall = $(this).attr("href");
        $("iframe").attr('src', "//www.youtube.com/embed/QEX0RlrBds4" + "?autoplay=0" + "&modestbranding=0" + "&showinfo=0" + "&rel=0");
        $('#videoModalArtsEarlyChildhood').foundation('open');
      }),
    attach: $(document).on("closed.zf.reveal", '[data-reveal]', function () {
        $('.arts-early-childhood iframe').attr('src', '');
        var ytc = $(".youtube-video").length;
        for (i=0; i < ytc; i++) {
          $(".youtube-video")[i].contentWindow.postMessage('{"event": "command", "func":"' + 'stopVideo' + '", "args":""}', '*');
        }
      }),
    attach: $("a.arts-career").on("click", function(event) {
        event.preventDefault();
        vall = $(this).attr("href");
        $("iframe").attr('src', "//www.youtube.com/embed/Yy0HSfZDPmA" + "?autoplay=0" + "&modestbranding=0" + "&showinfo=0" + "&rel=0");
        $('#videoModalArtsCareer').foundation('open');
      }),
    attach: $(document).on("closed.zf.reveal", '[data-reveal]', function () {
        $('.arts-career iframe').attr('src', '');
        var ytc = $(".youtube-video").length;
        for (i=0; i < ytc; i++) {
          $(".youtube-video")[i].contentWindow.postMessage('{"event": "command", "func":"' + 'stopVideo' + '", "args":""}', '*');
        }
      }),
    attach: $("a.arts-college").on("click", function(event) {
        event.preventDefault();
        vall = $(this).attr("href");
        $("iframe").attr('src', "//www.youtube.com/embed/PML0_vaRmHk" + "?autoplay=0" + "&modestbranding=0" + "&showinfo=0" + "&rel=0");
        $('#videoModalCollege').foundation('open');
      }),
    attach: $(document).on("closed.zf.reveal", '[data-reveal]', function () {
        $('.arts-college iframe').attr('src', '');
        var ytc = $(".youtube-video").length;
        for (i=0; i < ytc; i++) {
          $(".youtube-video")[i].contentWindow.postMessage('{"event": "command", "func":"' + 'stopVideo' + '", "args":""}', '*');
        }
      }),
  };

})(jQuery, Drupal);
